import difflib
import midiAnalysis


class DiffCode:
    IDENTICAL = 0  # starts with '  '
    TO_COMPARE = 1  # starts with '+ '
    REFERENCE = -1  # starts with '- '


class DifflibParser:
    def __init__(self, reference, to_compare):
        self.reference = reference
        self.to_compare = to_compare
        self.__diff = list(difflib.ndiff([str(i.pitch) for i in self.reference], [str(i.pitch) for i in self.to_compare]))
        self.__currentLineno = 0

        self.reference_only = 0
        self.tc_only = 0

    def __iter__(self):
        return self

    def __next__(self):  #
        result = {}

        if self.__currentLineno >= len(self.__diff):
            raise StopIteration

        current_line = self.__diff[self.__currentLineno]
        code = current_line[:2]
        if code == '  ':
            result['code'] = DiffCode.IDENTICAL
            result['properties'] = self.reference[self.__currentLineno - self.tc_only]
            result['note'] = midiAnalysis.midi2note(result['properties'].pitch)

        if code == '- ':
            result['code'] = DiffCode.REFERENCE
            self.reference_only += 1
            result['properties'] = self.reference[self.__currentLineno - self.tc_only]
            result['note'] = midiAnalysis.midi2note(result['properties'].pitch)

        elif code == '+ ':
            result['code'] = DiffCode.TO_COMPARE
            self.tc_only += 1
            result['properties'] = self.to_compare[self.__currentLineno - self.reference_only]
            result['note'] = midiAnalysis.midi2note(result['properties'].pitch)

        return result
