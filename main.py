import transcription
from difflibparser import *
from midiAnalysis import *
import pretty_midi

REFERENCE = r'assets/original.mid'
TO_COMPARE = r'assets/to_compare.mid'


def main():
    reference = pretty_midi.PrettyMIDI(REFERENCE)
    to_compare = pretty_midi.PrettyMIDI(TO_COMPARE)

    r_sorted_by_start = sort_notes(reference)
    tc_sorted_by_start = sort_notes(to_compare)
    difference = DifflibParser(r_sorted_by_start, tc_sorted_by_start)

    midi_evaluation = EvaluateMidis(difference)
    midi_evaluation.evaluate()
    midi_evaluation.print_all()

    '''
    plotter = Plotter()
    plotter.show(reference, to_compare, "comparison.html")
    '''


if __name__ == "__main__":
    #main()
    import comparetest
