import pyaudio
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from scipy.fft import rfft, rfftfreq
import time


# constants
CHUNK = 1024 * 2             # samples per frame
FORMAT = pyaudio.paInt16     # audio format (bytes per sample?)
CHANNELS = 1                 # single channel for microphone
RATE = 44100                 # samples per second


fig, ax = plt.subplots(1, figsize=(15, 7))
ax.set_xlim(20, RATE / 2)


# pyaudio class instance
p = pyaudio.PyAudio()

# stream object to get data from microphone
stream = p.open(
    format=FORMAT,
    channels=CHANNELS,
    rate=RATE,
    input=True,
    output=True,
    frames_per_buffer=CHUNK
)

def animate(i):
    data = stream.read(CHUNK)
    #time.sleep(1)
    data_np = np.frombuffer(data, dtype=np.int16)

    fft_spectrum = rfft(data_np)
    freq = rfftfreq(data_np.size, d=1.0 / RATE)

    mask = (freq >= 27.5) & (freq <= 4186) & (fft_spectrum >= 0) & (fft_spectrum <= 1000000)

    fft_spectrum = fft_spectrum[mask]
    freq = freq[mask]
    
    ax.clear()

    ax.set_xlim([0, 5000])
    ax.scatter(freq, fft_spectrum)
    

ani = animation.FuncAnimation(fig, animate, interval=0)
plt.show()
    
