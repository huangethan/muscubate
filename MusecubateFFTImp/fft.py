from scipy.fft import rfft, rfftfreq
from scipy.io.wavfile import read as read_wav
import numpy as np
import matplotlib.pyplot as plt
import math


class AudioProcessor():
	def __init__(self, file):
		self.file = file
		self.sampling_rate, self.data = read_wav(self.file)
		self.sound = self.data / 2.0**15
		self.signal = self.sound[:, 0]  # only use one channel

	def split_wav(self, time_start, time_end):
		return self.sampling_rate, self.signal[int(time_start * self.sampling_rate):int(time_end * self.sampling_rate)]

	def output_original(self):
		return self.sampling_rate, self.signal

class FFT():
	def __init__(self, sampling_rate, signal):
		self.sample_rate = sampling_rate
		self.signal = signal
		self.fft_spectrum = None
		self.freq = None

	def calibrate(self, calibration_file):
		fft_spectrum, freq = self.filter_frequencies(*FFT(*AudioProcessor(calibration_file).output_original()).fft(), 27.5, 4186)
		return np.max(fft_spectrum)

	def filter_frequencies(self, fft_spectrum, freq, freq_low, freq_high):
		new_fft_spectrum = []
		new_freq = []
		for i in range(len(freq)):
			if freq[i] > freq_low and freq[i] < freq_high:
				new_fft_spectrum.append(fft_spectrum[i])
				new_freq.append(freq[i])

		return np.array(new_fft_spectrum), np.array(new_freq)

	def filter_amplitude(self, fft_spectrum, freq, amplitude_low, amplitude_high):
		new_fft_spectrum = []
		new_freq = []
		for i in range(len(freq)):
			if fft_spectrum[i] > amplitude_low and fft_spectrum[i] < amplitude_high:
				new_fft_spectrum.append(fft_spectrum[i])
				new_freq.append(freq[i])

		return np.array(new_fft_spectrum), np.array(new_freq)
	
	def fft(self):
		fft_spectrum = rfft(self.signal)
		freq = rfftfreq(self.signal.size, d=1.0 / self.sample_rate)
		return fft_spectrum, freq
	
	def filtered_fft(self):
		self.fft_spectrum, self.freq = self.filter_frequencies(*self.fft(), 27.5, 4186)
		# self.filter_amplitude(*self.filter_frequencies(*self.fft(), 27.5, 4186), self.calibrate("quiet.wav"), self.calibrate("loud.wav"))

	def output_fft(self):
		return self.fft_spectrum, self.freq

	def plot_fft(self):
		#plt.plot(self.freq, self.fft_spectrum)
		plt.scatter(self.freq, self.fft_spectrum)
		plt.xlabel("frequency, Hz")
		plt.ylabel("Amplitude, units")
		plt.show()


def freq2note(frequency, sharp=True, a4=440):
    c0 = a4 * pow(2, -4.75)
    if sharp:
        name = ["C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"]
    else:
        name = ["C", "Db", "D", "Eb", "E", "F", "Gb", "G", "Ab", "A", "Bb", "B"]
    h = round(12 * math.log2(frequency / c0))
    octave = h // 12
    n = h % 12

    return name[n] + str(octave)


def main():
	file = AudioProcessor("sonata_processed.wav")
	fft = FFT(*file.split_wav(0, 10))
	fft.filtered_fft()

	fft.plot_fft()
	for i in range(len(fft.output_fft()[0])):
		print(freq2note(fft.output_fft()[1][i]))
	
if __name__ == "__main__":
	main()