"""
Converts a pretty midi sequence to a boket plot
"""
import math
import sys

import bokeh
import bokeh.plotting
from bokeh.colors import RGB
from bokeh.io import output_file
from bokeh.io import show
from bokeh.layouts import column
from bokeh.models import BoxAnnotation
from bokeh.models import ColumnDataSource
from bokeh.models import Range1d
from bokeh.models import Title

from pretty_midi import PrettyMIDI
from pretty_midi import TimeSignature

from typing import Optional


def midi2note(note, sharp=True, a4=440):
    frequency = (a4 / 32) * (2 ** ((note - 9) / 12))
    c0 = a4 * pow(2, -4.75)
    if sharp:
        name = ["C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"]
    else:
        name = ["C", "Db", "D", "Eb", "E", "F", "Gb", "G", "Ab", "A", "Bb", "B"]
    h = round(12 * math.log2(frequency / c0))
    octave = h // 12
    n = h % 12

    return name[n] + str(octave)


def get_time_signatures(pm: PrettyMIDI):
    if pm.time_signature_changes:
        time_signature = pm.time_signature_changes[0]
    else:
        time_signature = TimeSignature(4, 4, 0)

    return time_signature


def get_seconds_per_beat(pm: PrettyMIDI):
    if len(pm.get_beats()) >= 2:
        seconds_per_beat = pm.get_beats()[1] - pm.get_beats()[0]
    else:
        seconds_per_beat = 0.5

    return seconds_per_beat


def get_seconds_per_bar(pm: PrettyMIDI):
    if len(pm.get_downbeats()) >= 2:
        seconds_per_bar = pm.get_downbeats()[1] - pm.get_downbeats()[0]
    else:
        seconds_per_bar = 2.0

    return seconds_per_bar


def sort_notes(pm: PrettyMIDI):
    return sorted(pm.instruments[0].notes, key=lambda note: (note.start, note.pitch, note.end))


class EvaluateMidis:
    def __init__(self, midi_properties):
        self.midi_properties = list(midi_properties)
        self.common_counter = 0
        self.original_only_counter = 0
        self.to_compare_only_counter = 0
        self.percentage_right = 0
        self.percentage_wrong = 0
        self.reference_only = []
        self.to_compare_only = []

    def evaluate(self):
        for properties in self.midi_properties:
            if properties["code"] == 0:
                self.common_counter += 1
            if properties["code"] == -1:
                self.original_only_counter += 1
                self.reference_only.append(properties)
            if properties["code"] == 1:
                self.to_compare_only_counter += 1
                self.to_compare_only.append(properties)

        correct = self.common_counter + self.original_only_counter
        self.percentage_right = self.common_counter / correct
        self.percentage_wrong = (self.original_only_counter + self.to_compare_only_counter) / 2 / correct

    def raw_accuracy(self):
        return self.percentage_right, self.percentage_wrong

    def get_reference_only(self):
        return self.reference_only

    def get_to_compare_only(self):
        return self.to_compare_only

    def print_all(self):
        for properties in self.midi_properties:
            print(properties)


class Preset:
    """
    Preset class to configure the plotter bokeh plot output.
    """

    def __init__(self,
                 plot_width: int = 1500,
                 plot_height: int = 800,
                 row_height: Optional[int] = None,
                 title_text_font_size: str = "14px",
                 axis_label_text_font_size: str = "12px",
                 label_text_font_size: str = "10px",
                 label_text_font_style: str = "normal",
                 toolbar_location: Optional[str] = "right"):
        self.plot_width = plot_width
        self.plot_height = plot_height
        self.row_height = row_height
        self.title_text_font_size = title_text_font_size
        self.axis_label_text_font_size = axis_label_text_font_size
        self.label_text_font_size = label_text_font_size
        self.label_text_font_style = label_text_font_style
        self.toolbar_location = toolbar_location

    def PRESET_4K(self):
        return Preset(plot_width=3840,
                      row_height=100,
                      title_text_font_size="65px",
                      axis_label_text_font_size="55px",
                      label_text_font_size="40px")


class Plotter:
    """
    Plotter class with plot size, time scaling and live reload
    configuration.
    """

    _MAX_PITCH = 127
    _MIN_PITCH = 0

    def __init__(self):
        self._preset = Preset()
        self._bar_fill_alphas = [0.25, 0.05]

    def initialize_data(self, pm: PrettyMIDI, reference: bool):
        data = dict(reference=[], note=[], top=[], bottom=[], left=[], right=[], duration=[], velocity=[], color=[])

        pitch_min = None
        pitch_max = None
        first_note_start = None
        last_note_end = None
        if reference:
            color = RGB(r=240, g=123, b=123, a=0.2)
        else:
            color = RGB(r=70, g=100, b=200, a=0.2)

        for note in pm.instruments[0].notes:
            pitch_min = min(pitch_min or self._MAX_PITCH, note.pitch)
            pitch_max = max(pitch_max or self._MIN_PITCH, note.pitch)
            note_start = note.start
            note_end = note.start + (note.end - note.start)
            data["top"].append(note.pitch)
            data["note"].append(midi2note(note.pitch))
            data["bottom"].append(note.pitch + 1)
            data["left"].append(note_start)
            data["right"].append(note_end)
            data["duration"].append(note_end - note_start)
            data["velocity"].append(note.velocity)
            data["color"].append(color)
            if reference:
                data["reference"].append("Reference Score")
            else:
                data["reference"].append("To Compare Score")
            first_note_start = min(first_note_start or sys.maxsize, note_start)
            last_note_end = max(last_note_end or 0, note_end)

        pitch_min = min(self._MAX_PITCH, pitch_min)
        pitch_max = max(self._MIN_PITCH, pitch_max)

        return data, pitch_min, pitch_max

    def plot(self, reference: PrettyMIDI, to_compare: PrettyMIDI):
        preset = self._preset
        r_bpm = reference.estimate_tempo()
        tc_bpm = reference.estimate_tempo()
        plot = bokeh.plotting.figure(tools="reset,wheel_zoom,pan", toolbar_location=preset.toolbar_location)

        reference_hover = bokeh.models.HoverTool()
        reference_hover.tooltips = {
            "Reference": "@reference",
            "Note": "@note",
            "Velocity": "@velocity",
            "Start Time": "@left",
            "End Time": "@right"}

        to_compare_hover = bokeh.models.HoverTool()
        to_compare_hover.tooltips = {
            "Reference": "@reference",
            "Note": "@note",
            "Velocity": "@velocity",
            "Start Time": "@left",
            "End Time": "@right"}

        r_data, r_pitch_min, r_pitch_max = self.initialize_data(reference, reference=True)
        tc_data, tc_pitch_min, tc_pitch_max = self.initialize_data(to_compare, reference=False)

        pitch_min = min(r_pitch_min, tc_pitch_min)
        pitch_max = max(r_pitch_max, tc_pitch_max)
        pitch_range = pitch_max + 1 - pitch_min

        r_source = ColumnDataSource(data=r_data)
        tc_source = ColumnDataSource(data=tc_data)

        plot.quad(left="left",
                  right="right",
                  top="top",
                  bottom="bottom",
                  line_alpha=1,
                  line_color="black",
                  color="color",
                  source=r_source,
                  name="reference")

        plot.quad(left="left",
                  right="right",
                  top="top",
                  bottom="bottom",
                  line_alpha=1,
                  line_color="black",
                  color="color",
                  source=tc_source,
                  name="to_compare")

        reference_hover.renderers = plot.select(name="reference")
        to_compare_hover.renderers = plot.select(name="to_compare")

        plot.add_tools(reference_hover)
        plot.add_tools(to_compare_hover)

        r_time_signature = get_time_signatures(reference)
        tc_time_signature = get_time_signatures(to_compare)

        r_seconds_per_beat = get_seconds_per_beat(reference)
        tc_seconds_per_beat = get_seconds_per_beat(to_compare)

        r_seconds_per_bar = get_seconds_per_bar(reference)
        tc_seconds_per_bar = get_seconds_per_bar(to_compare)

        bar_count = 0
        for bar_time in reference.get_downbeats():
            fill_alpha_index = bar_count % len(self._bar_fill_alphas)
            fill_alpha = self._bar_fill_alphas[fill_alpha_index]
            box = BoxAnnotation(left=bar_time,
                                right=bar_time + r_seconds_per_bar,
                                fill_color="gray",
                                fill_alpha=fill_alpha,
                                line_color="black",
                                line_width=2,
                                line_alpha=0.5,
                                level="underlay")
            plot.add_layout(box)
            bar_count += 1

        for beat_time in reference.get_beats():
            box = BoxAnnotation(left=beat_time,
                                right=beat_time + r_seconds_per_beat,
                                fill_color=None,
                                line_color="black",
                                line_width=1,
                                line_alpha=0.4,
                                level="underlay")
            plot.add_layout(box)

        # Configure x axis
        plot.xaxis.axis_label = "SECONDS"
        plot.xaxis.axis_label_text_font_size = preset.axis_label_text_font_size
        plot.xaxis.ticker = bokeh.models.SingleIntervalTicker(interval=1)
        plot.xaxis.major_label_text_font_size = preset.label_text_font_size
        plot.xaxis.major_label_text_font_style = preset.label_text_font_style

        # Configure y axis
        plot.yaxis.axis_label = "PITCH"
        plot.yaxis.axis_label_text_font_size = preset.axis_label_text_font_size
        plot.yaxis.ticker = bokeh.models.SingleIntervalTicker(interval=1)

        plot.xgrid.grid_line_color = None

        # Configure the plot size and range
        plot_title_text = \
            f"""MIDI Compare:
            Reference:  {int(r_bpm)} BPM, {r_time_signature.numerator}/{r_time_signature.denominator}
            To Compare: {int(tc_bpm)} BPM, {tc_time_signature.numerator}/{tc_time_signature.denominator}"""
        plot.title = Title(text=plot_title_text, text_font_size=preset.title_text_font_size)
        plot.plot_width = preset.plot_width
        if preset.row_height:
            plot.plot_height = pitch_range * preset.row_height
        else:
            plot.plot_height = preset.plot_height
        plot.y_range = Range1d(pitch_min, pitch_max + 1, bounds="auto")
        plot.min_border_right = 50

        layout = column(plot)

        return layout

    def show(self, pm: PrettyMIDI, to_compare: PrettyMIDI, filepath: str):
        plot = self.plot(pm, to_compare)
        output_file(filepath)
        show(plot)
        return plot


class Plotter2:
    _MAX_PITCH = 127
    _MIN_PITCH = 0

    def __init__(self, difference):
        self._preset = Preset()
        self._bar_fill_alphas = [0.25, 0.05]
        self.difference = list(difference)

    def initialize_data(self):
        data = dict(reference=[], note=[], top=[], bottom=[], left=[], right=[], duration=[], velocity=[], color=[])

        pitch_min = None
        pitch_max = None
        first_note_start = None
        last_note_end = None
        '''
        if reference:
            color = RGB(r=240, g=123, b=123, a=0.2)
        else:
            color = RGB(r=70, g=100, b=200, a=0.2)
        '''

        for note in self.difference:
            pitch_min = min(pitch_min or self._MAX_PITCH, note["properties"].pitch)
            pitch_max = max(pitch_max or self._MIN_PITCH, note["properties"].pitch)
            note_start = note["properties"].start
            note_end = note["properties"].start + (note["properties"].end - note["properties"].start)
            data["top"].append(note["properties"].pitch)
            data["note"].append(midi2note(note["properties"].pitch))
            data["bottom"].append(note["properties"].pitch + 1)
            data["left"].append(note_start)
            data["right"].append(note_end)
            data["duration"].append(note_end - note_start)
            data["velocity"].append(note["properties"].velocity)

            data["color"].append(color)

            first_note_start = min(first_note_start or sys.maxsize, note_start)
            last_note_end = max(last_note_end or 0, note_end)

        pitch_min = min(self._MAX_PITCH, pitch_min)
        pitch_max = max(self._MIN_PITCH, pitch_max)

        return data, pitch_min, pitch_max


