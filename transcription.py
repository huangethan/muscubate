from piano_transcription_inference import PianoTranscription, sample_rate, load_audio


def transcriber(audio_path, device, pretrained_path):
    (audio, _) = load_audio(audio_path, sr=sample_rate, mono=True)
    transcribed = PianoTranscription(checkpoint_path=pretrained_path, device=device)
    modified_file = audio_path.split("/")[-1].split(".")[0] + ".mid"
    transcribed_dict = transcribed.transcribe(audio, modified_file)

    '''
    transcription.transcriber(r'C:\home\ehuang\musecubate\assets\wav\real_life_test_mono.wav', 'cuda', r'C:\home\ehuang\musecubate\model.pth')
    '''
