# Import the necessary libraries
import pretty_midi
import numpy as np
from dtaidistance import dtw
from dtaidistance import dtw_visualisation as dtwvis

# Load the MIDI files using the python-midi library
midi1 = pretty_midi.PrettyMIDI(r'assets/original.mid')
midi2 = pretty_midi.PrettyMIDI(r'assets/to_compare.mid')

# Extract the note and timing data from the MIDI files
pitches1 = [np.mean(note.pitch) for note in midi1.instruments[0].notes]
pitches2 = [np.mean(note.pitch) for note in midi2.instruments[0].notes]

# Calculate the DTW distance between the two sequences
path = dtw.warping_path(pitches1, pitches2)

dtwvis.plot_warping(pitches1, pitches2, path, filename="warp.png")


for index in path:
    print(pitches1[index[0]], pitches2[index[1]])
