<div id="top"></div>

<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://gitlab.com/huangethan/muscubate">
    <img src="image/logo.png" alt="Logo" width="500" height="80">
  </a>

<h3 align="center">Musecubate</h3>

  <p align="center">
    An attempt at creating an app that detects wrong notes from a music piece.
    <br />
    <a href="https://gitlab.com/huangethan/muscubate"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="https://gitlab.com/huangethan/muscubate">View Demo</a>
    ·
    <a href="https://gitlab.com/huangethan/muscubate">Report Bug</a>
    ·
    <a href="https://gitlab.com/huangethan/muscubate">Request Feature</a>
  </p>
</div>



<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgments">Acknowledgments</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project

[![Product Name Screen Shot][product-screenshot]](https://gitlab.com/huangethan/muscubate)

Musecubate is a project in attempt to correct a player's wrong notes when sight-reading music. 
The story of the creation of Muscubate is that I have failed DipABRSM sight-reading multiple times, which made me wonder "if there was an app that could help detect incorrect notes".

Currently, the closest match to this idea is Simply Piano or Smart Music, but they come with some limitations.
Both these applications have limitations such as the inability to detect chords or only having a select sample of pieces to play from.

Musecubate in theory hopefully will solve these limitations.

<p align="right">(<a href="#top">back to top</a>)</p>


<!-- GETTING STARTED -->
## Getting Started
### Prerequisites
* [Python](https://www.python.org/)
* [Pretty Midi](https://github.com/craffel/pretty-midi)
  ```sh
  pip install pretty_midi
  ```
* [Bokeh](http://docs.bokeh.org/en/latest/)
  ```sh
  pip install pretty_midi
  ```
* [Piano Transcription Inference](https://github.com/qiuqiangkong/piano_transcription_inference)
  ```sh
  pip install piano_transcription_inference
  ```

### Installation
1. Download Python
2. Clone the repo
   ```sh
   git clone git@gitlab.com:huangethan/muscubate.git
   ```
3. Install dependencies
   
<p align="right">(<a href="#top">back to top</a>)</p>


<!-- USAGE EXAMPLES -->
## Usage

Use this space to show useful examples of how a project can be used. Additional screenshots, code examples and demos work well in this space. You may also link to more resources.

_For more examples, please refer to the [Documentation](https://example.com)_

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- ROADMAP -->
## Roadmap

- [ ] Convert MP3 to MIDI file
- [ ] Compare MIDI files
  - [ ] Find Differences/Uniqueness and Similarities
- [ ] Evaluation
    - [ ] Get Note Accuracy as Percentage
    - [ ] Get Tempo Changes and Compare
    - [ ] Find corresponding error in one MIDI file in the other file
- [ ] Frontend
  - [ ] Create webapp
  - [ ] Graphing and Displaying Errors
  - [ ] Playback few seconds before errors

See the [open issues](https://github.com/github_username/repo_name/issues) for a full list of proposed features (and known issues).

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- CONTRIBUTING -->
## Contributing

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement".
Don't forget to give the project a star! Thanks again!

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE.txt` for more information.

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- CONTACT -->
## Contact

Ethan Huang - [@twitter_handle](https://twitter.com/twitter_handle) - ethanhuang301@gmail.com

Project Link: [https://github.com/github_username/repo_name](https://github.com/github_username/repo_name)

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- ACKNOWLEDGMENTS -->
## Acknowledgments
Will update licensing to accomdate for the following repos.
* [Piano Transcription Inference](https://github.com/bytedance/piano_transcription)
  * Transcribes MP3 into MIDI format
* [Difflibparser](https://github.com/yebrahim/difflibparser)
  * Finds difference between two files

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[product-screenshot]: image/screenshot.png
